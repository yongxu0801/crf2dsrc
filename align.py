# -*- coding: utf-8 -*-

"""
This script reads a CRF model and align a bitext in the XCES format.

Arguments :
- Source language bitext
- Target language bitext
- Partial alignment document
- CRF parameter file 
"""

from codecs import open
from sys import argv
import graph
import train
import xces 

def findGap(xcesSrc, xcesTrg, xcesAlign, threshold=0.9999) :
  """
  Given a bitext encoded in the XCES format and an alignment link
  document for the bitext, find the gaps to be aligned.
  """
  dictSentSrc = xcesSrc.getSentenceDict()
  dictSentTrg = xcesTrg.getSentenceDict()
  id2IndSrc = xcesSrc.dictID2Ind
  id2IndTrg = xcesTrg.dictID2Ind

  listGap = []
  prevSrc = 0
  prevTrg = 0
  for link in xcesAlign.listLink :
    srcID, trgID, conf = link
    if float(conf) < threshold : continue
    if srcID == u"" or trgID == u"" : continue
    firstIndSrc = id2IndSrc[srcID.split()[0]]
    lastIndSrc = id2IndSrc[srcID.split()[-1]]
    firstIndTrg = id2IndTrg[trgID.split()[0]]
    lastIndTrg = id2IndTrg[trgID.split()[-1]]
    #print firstIndSrc, lastIndSrc, firstIndTrg, lastIndTrg
    #print firstIndSrc, lastIndSrc, firstIndTrg, lastIndTrg
    if firstIndSrc != prevSrc + 1 or firstIndTrg != prevTrg + 1 :
      gap = [range(prevSrc + 1, firstIndSrc), range(prevTrg + 1, firstIndTrg)]
      #print gap
      listGap.append(gap)
    prevSrc = lastIndSrc
    prevTrg = lastIndTrg

  lastIndSrc = max(dictSentSrc.keys())
  lastIndTrg = max(dictSentTrg.keys())
  #print prevSrc, prevTrg, lastIndSrc, lastIndTrg
  if prevSrc < lastIndSrc or prevTrg < lastIndTrg :
    gap = [range(prevSrc + 1, lastIndSrc), range(prevTrg + 1, lastIndTrg)]
    listGap.append(gap)
  #print listGap
  return listGap

def crfAlignGap(crf, xcesSrc, xcesTrg, srcSentInds, trgSentInds, ttable1={}, ttable2={}, frequentSrc=[], frequentTrg=[]) :
  """ Use the CRF alignment model to align an unaligned gap. """
  g = graph.Graph(srcSentInds, trgSentInds)
  obs = train.text2obs(xcesSrc, xcesTrg, srcSentInds, trgSentInds, ttable1, ttable2, frequentSrc, frequentTrg)
  pred, localBel = crf.prediction(g, obs)
  name = u"Test"
  Y = {}
  for x in pred : Y[x] = 1
  record = None
  logLocalPot = graph.makeLocalPot(g, crf.localWeights, obs)
  train.printInfo(xcesSrc, xcesTrg, name, Y, obs, pred, logLocalPot, localBel, record, writeRecord=False) 
  #for (indSrc, indTrg) in product(range(len(srcSents)), range(len(trgSents))) :
  #  print srcSents[indSrc]
  #  print trgSents[indTrg]
  #  print pred[indSrc, indTrg]
  #  print "\n\n"

def SortListBisegment(list_bisegment, limitSrc):
  """
  Sort the final list of links.
  """
  list_non_empty_src = filter(lambda b : b[0] != [], list_bisegment);
  list_empty_src = filter(lambda b : b[0] == [], list_bisegment);
  list_non_empty_src.sort(key = lambda b : b[0][0]);
  list_empty_src.sort(key = lambda b : b[1][0]);

  if len(list_empty_src) > 0 and list_empty_src[0][1][0] == 1 :
    list_non_empty_src.insert(0, list_empty_src[0])
  for bisegment in list_empty_src:
    for ind in xrange(len(list_non_empty_src)):
      if list_non_empty_src[ind][1] != [] and \
         bisegment[1][0] == list_non_empty_src[ind][1][-1] + 1 :
        list_non_empty_src.insert(ind + 1, bisegment);
        break

  listRes = []
  for bisegment in list_non_empty_src :
    if bisegment[0] == [] : listRes.append(bisegment)
    elif bisegment[0][0] > limitSrc : break
    else : listRes.append(bisegment)

  return listRes


  
if __name__ == "__main__" :
  try :
    paramFile = argv[4]
    xcesSrc = xces.xcesTextFile(argv[1])
    xcesTrg = xces.xcesTextFile(argv[2])
    partialAlign = xces.xcesAlignFile(argv[3])
  except :
    print(u"""
Arguments :
- Source language bitext
- Target language bitext
- Partial alignment document
- CRF parameter file 
""".encode("utf-8"))
    exit()

  threshold = 0.9999
  if len(argv) == 6 :
    threshold = float(argv[5])
    
  dictWordCodeTrg, frequentTrg = train.word2code("/people/yong/TransRead/crf2dSrc/Europarl/ep_v7_low.fr.vcb")
  dictWordCodeSrc, frequentSrc = train.word2code("/people/yong/TransRead/crf2dSrc/Europarl/ep_v7_low.en.vcb")
  ttable1 = train.code2prob("/people/yong/TransRead/crf2dSrc/Europarl/ep_v7_low.mgiza/fr2en.t3.final", dictWordCodeTrg, dictWordCodeSrc)
  ttable2 = train.code2prob("/people/yong/TransRead/crf2dSrc/Europarl/ep_v7_low.mgiza/en2fr.t3.final", dictWordCodeSrc, dictWordCodeTrg)

  dictSentSrc = xcesSrc.getSentenceDict()
  dictSentTrg = xcesTrg.getSentenceDict()
  crf = graph.CRF()
  crf.readModel(paramFile)
  listGap = findGap(xcesSrc, xcesTrg, partialAlign, threshold)
  finalLinks = [ ([xcesSrc.dictID2Ind[x]], [xcesTrg.dictID2Ind[y]]) for x, y, conf in moore.listLink if conf >= threshold]  
  for gap in listGap :
    srcInds, trgInds = gap
    print u" ".join([unicode(xcesSrc.dictInd2ID[ind]) for ind in srcInds]) + "\t --- \t" + u" ".join([unicode(xcesTrg.dictInd2ID[ind]) for ind in trgInds])
    if srcInds == [] or trgInds == [] : continue
    crfAlignGap(crf, xcesSrc, xcesTrg, srcInds, trgInds, ttable1={}, ttable2={}, frequentSrc=[], frequentTrg=[]) 
    print "\n\n"
    
