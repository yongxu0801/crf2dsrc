# -*- coding: utf-8 -*-

"""
This one aligns a source XCES and a target XCES.
The CRF is trained on existing alignments.

Arguments :
- source XCES
- target XCES
- CRF model file
"""

from itertools import product
from codecs import open
from sys import argv
import graph
import xces
import numpy as np
import getFeature as gf
from sklearn import linear_model
import datetime
import heapq

def stringCRFWeights(c, dis=10) :
  """ CRF weights values. """
  s = u"" + \
    u"Weights for : " + " \n" + \
    u"length diff / length src      " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[0:dis]])  + " \n" + \
    u"length diff / length trg      " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[dis:2*dis]]) + " \n" + \
    u"num copy / num word src       " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[2*dis:3*dis]]) + " \n" + \
    u"num copy / num word trg       " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[3*dis:4*dis]]) + " \n" + \
    u"num cognate / num word src    " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[4*dis:5*dis]]) + " \n" + \
    u"num cognate / num word trg    " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[5*dis:6*dis]]) + " \n" + \
    u"indice ratio difference       " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[6*dis:7*dis]]) + " \n" + \
    u"IBM 1 lexical probability src " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[7*dis:8*dis]]) + " \n" + \
    u"IBM 1 lexical probability trg " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[8*dis:9*dis]]) + " \n" + \
    u"IBM 1 lexical probability mean" + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[9*dis:10*dis]]) + " \n" + \
    u"Footprint coverage src        " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[10*dis:11*dis]]) + " \n" + \
    u"Footprint coverage trg        " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[11*dis:12*dis]]) + " \n" + \
    u"num shared rare word src      " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[12*dis:12*dis+5]]) + " \n" + \
    u"num shared rare word trg      " + u"".join([u"{0:8.4f}".format(x) for x in c.localWeights[12*dis+5:12*dis+10]]) + " \n" + \
    u"State transition :            " + u"".join([u"{0:8.4f}".format(x) for x in c.pairPot.flatten()[0:4]]) + " \n" + \
    u"\n"
  return s

def stringObsNode(xcesSrc, xcesTrg, x, y, obsNode, dis=10) :
  """ Feature values. """
  s = u"" + \
    u"{0}".format(xcesSrc.findSentenceByInd(x))  + " \n" + \
    u"{0}".format(xcesTrg.findSentenceByInd(y))  + " \n" + \
    u"length diff / length src      " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[0:dis]])  + " \n" + \
    u"length diff / length trg      " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[dis:2*dis]]) + " \n" + \
    u"num copy / num word src       " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[2*dis:3*dis]]) + " \n" + \
    u"num copy / num word trg       " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[3*dis:4*dis]]) + " \n" + \
    u"num cognate / num word src    " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[4*dis:5*dis]]) + " \n" + \
    u"num cognate / num word trg    " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[5*dis:6*dis]]) + " \n" + \
    u"indice ratio difference       " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[6*dis:7*dis]]) + " \n" + \
    u"IBM 1 lexical probability src " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[7*dis:8*dis]]) + " \n" + \
    u"IBM 1 lexical probability trg " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[8*dis:9*dis]]) + " \n" + \
    u"IBM 1 lexical probability mean" + u"  ".join([u"{0}".format(int(x)) for x in obsNode[9*dis:10*dis]]) + " \n" + \
    u"Footprint coverage src        " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[10*dis:11*dis]]) + " \n" + \
    u"Footprint coverage trg        " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[11*dis:12*dis]]) + " \n" + \
    u"num shared rare word src      " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[12*dis:12*dis+5]]) + " \n" + \
    u"num shared rare word trg      " + u"  ".join([u"{0}".format(int(x)) for x in obsNode[12*dis+5:12*dis+10]]) + " \n" + \
    u"\n"
  return s

def printInfo(xcesSrc, xcesTrg, name, Y, obs, pred, logLocalPot, localBel, record, writeRecord=False) :
  """ Print information about a prediction """
  print u"\n{0}\n".format(name).encode("utf-8")
  if writeRecord : record.write(u"\n{0}\n".format(name).encode("utf-8"))
  truePos = 0.0 # True positive predicted labels
  positive = 0.0 # Total count of true labels
  falsePos = 0.0 # False positives
  falseNeg = 0.0 # False negative

  for (x, y) in sorted(pred) :
    if pred[x, y] == 1 and Y[x, y] == 1 : continue
    if Y[x, y] == 2 and pred[x, y] == 1 :
      positive += 1
      falseNeg += 1
      print "False negative : "
      if writeRecord : record.write(u"False negative : \n".encode("utf-8"))
    if Y[x, y] == 2 and pred[x, y] == 2 :
      truePos += 1
      positive += 1
      print "True positive : "
      if writeRecord : record.write(u"True positive : \n".encode("utf-8"))
    if Y[x, y] == 1 and pred[x, y] == 2 :
      falsePos += 1
      print "False positive : "
      if writeRecord : record.write(u"False positive : \n".encode("utf-8"))    
    print u"{0}, {1}, pred={2}, label={3}".format(xcesSrc.dictInd2ID[x], xcesTrg.dictInd2ID[y], pred[x,y], Y[x,y]).encode("utf-8")
    print u"Local potential : {0:8.4f}  {1:8.4f}".format(logLocalPot[x,y][0], logLocalPot[x, y][1]).encode("utf-8")
    print u"Local belief    : {0:8.4f}  {1:8.4f}".format(localBel[x, y][0], localBel[x,y][1]).encode("utf-8")
    print stringObsNode(xcesSrc, xcesTrg, x, y, obs[x,y]).encode("utf-8")
    if writeRecord :
      record.write(u"{0}, {1}, pred={2}, label={3}\n".format(xcesSrc.dictInd2ID[x], xcesTrg.dictInd2ID[y], pred[x, y], Y[x, y]).encode("utf-8"))
      record.write(u"Local potential : {0:8.4f}  {1:8.4f}\n".format(logLocalPot[x, y][0], logLocalPot[x,y][1]))
      record.write(u"Local belief    : {0:8.4f}  {1:8.4f}\n".format(localBel[x,y][0], localBel[x,y][1]))
      record.write(stringObsNode(xcesSrc, xcesTrg, x, y, obs[x,y]).encode("utf-8"))
  if writeRecord : record.write(u"=====================================\n".encode("utf-8"))

  return (truePos, positive, falsePos, falseNeg)

def label2links(dictLabel, graph, xcesSrc, xcesTrg) :
  """
  Turn the 2D grid representation to the list of links representation.
  This is a connected component problem. Use DFS.
  """
  def dfsFrom(marked, graph, dictLabel, inds, indt, sets, sett, ms, mt) :
    marked.add((inds, indt))
    sets.add(inds)
    sett.add(indt)
    ms.add(inds)
    mt.add(indt)
    for (nborS, nborT) in graph.getNeighbours(inds, indt) :
      if dictLabel[(nborS, nborT)] == 1 : continue
      if (nborS, nborT) not in marked :
        dfsFrom(marked, graph, dictLabel, nborS, nborT, sets, sett, ms, mt) 
       
  ind2IDSrc = xcesSrc.dictInd2ID
  ind2IDTrg = xcesTrg.dictInd2ID
  marked = set([])  # marked indices
  markedIndSrc = set([]) # marked source indices
  markedIndTrg = set([]) # marked target indices
  listLink = []
  for (inds, indt) in sorted(dictLabel) :
    if dictLabel[(inds, indt)] == 1 : continue
    if (inds, indt) in marked : continue
    setS = set([])  # source indices in this component
    setT = set([])  # target indices in this component
    dfsFrom(marked, graph, dictLabel, inds, indt, setS, setT, markedIndSrc, markedIndTrg)
    listS = sorted(list(setS))
    listT = sorted(list(setT))
    listLink.append((listS, listT)) # the link (thus the component) found in this round

  # Add null links, and make all links in order
  unmarkedIndSrc = set(graph.srcInds) - markedIndSrc
  unmarkedIndTrg = set(graph.trgInds) - markedIndTrg
  for indSrc in unmarkedIndSrc :
    listLink.append(([indSrc], []))
  listLink.sort()
  for indTrg in sorted(list(unmarkedIndTrg)) :
    if indTrg == 0 : 
      listLink.insert(0, ([], [0]))
      continue
    for indLink, link in enumerate(listLink) :
      src, trg = link
      if trg == [] : continue
      if trg[-1] == indTrg - 1 :
        listLink.insert(indLink + 1, ([], [indTrg]))
        break

  # Turn the links into xcesAlign format, return the links
  res = []
  for link in listLink :
    src, trg = link
    listS = [ind2IDSrc[ind] for ind in src]
    listT = [ind2IDTrg[ind] for ind in trg]
    print "{0};{1}".format(" ".join(listS), " ".join(listT))
    res.append((listS, listT))

  return res

def align2label(xcesSrc, xcesTrg, xcesAlign) :
  """
  Turn xcesAlign format into 2D grid represenation. 
  The label for each node is 1 (not linked) or 2 (linked).
  """
  ID2IndSrc = xcesSrc.getSentIDIndex()
  ID2IndTrg = xcesTrg.getSentIDIndex()
  dictLabel = {}
  for (indSrc, indTrg) in product(sorted(ID2IndSrc.values()), sorted(ID2IndTrg.values())) :
    dictLabel[(indSrc, indTrg)] = 1
  for link in xcesAlign.listLink :
    idSrc, idTrg, _ = link
    if idSrc == "" or idTrg == "" : continue
    for ids in idSrc.split() :
      inds = ID2IndSrc[ids]
      for idt in idTrg.split() :
        indt = ID2IndTrg[idt]
        dictLabel[(inds, indt)] = 2
  return dictLabel

def text2obs(xcesSrc, xcesTrg, srcSentInds, trgSentInds, ttable1={}, ttable2={}, frequentSrc=[], frequentTrg=[], Y={}) :
  """ Turn raw text into feature representations. """
  dictObs = {}
  srcSentInds.sort()
  trgSentInds.sort()
  for (indSrc, indTrg) in product(srcSentInds, trgSentInds) :
    if Y == {} :
      dictObs[(indSrc, indTrg)] = ConstructOneExample(xcesSrc, xcesTrg, indSrc, indTrg, ttable1, ttable2, frequentSrc, frequentTrg)
    else :
      dictObs[(indSrc, indTrg)] = ConstructOneExample(xcesSrc, xcesTrg, indSrc, indTrg, ttable1, ttable2, frequentSrc, frequentTrg, Y[indSrc, indTrg])
  return dictObs

def readTTable(IBM1File) :
  """ Read translation table from Giza output. """
  #from time import asctime
  #print u"Begin read ttable {0}".format(asctime()).encode("utf-8")
  ttable = {}
  dmax = {}
  for line in open(IBM1File) :
    line = line.strip().decode("utf-8").lower()
    tmp = line.split()
    if len(tmp) != 3 : continue
    sw = tmp[0]
    tw = tmp[1]
    prob = float(tmp[2])
    if sw not in ttable : ttable[sw] = {}
    ttable[sw][tw] = prob
  #print u"End read ttable {0}".format(asctime()).encode("utf-8")
  return ttable

def word2code(vcb, size=50) :
  """ Read the vocabulary file of Giza. Also record the most frequent 50 words."""
  frequent = []
  dictWordCode = {0:u"null", u"null":0}

  for l in open(vcb) :
    l = l.strip().decode("utf-8")
    l = l.split()
    if len(l) != 3 : continue
    code = int(l[0])
    word = l[1]
    fre = int(l[2])
    if len(frequent) < size :
      heapq.heappush(frequent, (fre, code))
    else :
      heapq.heappushpop(frequent, (fre, code))
    dictWordCode[code] = word

  return dictWordCode, [dictWordCode[x[1]] for x in frequent]

def code2prob(ibm1, wordCode1, wordCode2) :
  """ Read GIZA code-code lexical probability file. Store actual words. """
  ttable = {}
  for l in open(ibm1) :
    l = l.strip().decode("utf-8")
    l = l.split()
    if len(l) != 3 : continue
    code1 = int(l[0])
    code2 = int(l[1])
    word1 = wordCode1[code1]
    word2 = wordCode2[code2]
    if word1 not in ttable :
      ttable[word1] = {}
    ttable[word1][word2] = float(l[2])

  return ttable

def dictToArray(d) :
  row, col = sorted(d.keys())[-1]
  a = np.zeros(i(row, col))
  for x,y in d :
    a[x, y] = d[x,y]
  return a

def ConstructOneExample(xcesSrc, xcesTrg, indSrc, indTrg, ttable1={}, ttable2={}, frequentSrc=[], frequentTrg=[], y=None) :
  """ Construct the feature vector on one node. """
  sentSrc = xcesSrc.findSentenceByInd(indSrc).lower()
  sentTrg = xcesTrg.findSentenceByInd(indTrg).lower()
  prevSrc = xcesSrc.findSentenceByInd(indSrc-1).lower()
  prevTrg = xcesTrg.findSentenceByInd(indTrg-1).lower()
  nextSrc = xcesSrc.findSentenceByInd(indSrc+1).lower()
  nextTrg = xcesTrg.findSentenceByInd(indTrg+1).lower()
  f = []

  # src : trg
  f += gf.fLength(sentSrc, sentTrg) # 20
  f += gf.fCopy(sentSrc, sentTrg) # 20
  f += gf.fCognate(sentSrc, sentTrg) # 20 
  f += gf.fIndex(indSrc, indTrg, xcesSrc.numSent, xcesTrg.numSent) # 10
  f += gf.fLexicalProb(sentSrc, sentTrg, ttable1, ttable2, frequentSrc, frequentTrg) # 30
  f += gf.fFingerprint(xcesSrc, xcesTrg, indSrc, indTrg, ttable1, ttable2, frequentSrc, frequentTrg) # 20
  f += gf.fShareRareWord(xcesSrc, xcesTrg, sentSrc, sentTrg) # 10
  
  """ 
  # src : (trg - 1, trg)
  f += gf.fLength(sentSrc, prevTrg + " " + sentTrg)
  f += gf.fCopy(sentSrc, prevTrg + " " + sentTrg)
  f += gf.fCognate(sentSrc, prevTrg + " " + sentTrg)
  f += gf.fIndex(indSrc, indTrg-1, xcesSrc.numSent, xcesTrg.numSent) # 30
  f += gf.fLexicalProb(sentSrc, prevTrg + " " + sentTrg, ttable1, ttable2)
  f += gf.fShareRareWord(xcesSrc, xcesTrg, sentSrc, prevTrg + " " + sentTrg)

  # (src - 1, src) : trg
  f += gf.fLength(prevSrc + " " + sentSrc, sentTrg) # 12 
  f += gf.fCopy(prevSrc + " " + sentSrc, sentTrg) # 25
  f += gf.fCognate(prevSrc + " " +sentSrc, sentTrg) # 25
  f += gf.fIndex(indSrc-1, indTrg, xcesSrc.numSent, xcesTrg.numSent) # 30
  f += gf.fLexicalProb(prevSrc + " " + sentSrc, sentTrg, ttable1, ttable2)
  #f += gf.fFingerprint(xcesSrc, xcesTrg, indSrc, indTrg, ttable1, ttable2)
  f += gf.fShareRareWord(xcesSrc, xcesTrg, prevSrc + " " + sentSrc, sentTrg)
  """
  """
  # src : (trg, trg + 1)
  f += gf.fLength(sentSrc, sentTrg + " " + nextTrg )
  f += gf.fCopy(sentSrc, sentTrg  + " " + nextTrg)
  f += gf.fCognate(sentSrc, sentTrg + " " + nextTrg)
  f += gf.fLexicalProb(sentSrc, sentTrg + " " + nextTrg, ttable1, ttable2)
  f += gf.fShareRareWord(xcesSrc, xcesTrg, sentSrc, sentTrg + " " + nextTrg)  

  # (src, src + 1) : trg
  f += gf.fLength(sentSrc + " " + nextSrc, sentTrg) # 12 
  f += gf.fCopy(sentSrc + " " + nextSrc, sentTrg) # 25
  f += gf.fCognate(sentSrc + " " + nextSrc, sentTrg) # 25
  f += gf.fLexicalProb(sentSrc + " " + nextSrc, sentTrg, ttable1, ttable2)
  f += gf.fShareRareWord(xcesSrc, xcesTrg, sentSrc + " " + nextSrc, sentTrg)
  """
  
  f = [float(f_i) for f_i in f]
  
  f.append(1.0)
  return np.array(f)

if __name__ == "__main__" :
  try :
    output = argv[1]
  except :
    print(u"""
Arguments :
- One output file to store the crf parameters
""".encode("utf-8"))
    exit()

  dataDir = "/people/yong/TransRead/crf2d/books/"
  #dataDir = "/people/yong/TransRead/crf2d/MiddleTrain/"
  aligned = [\
              ("abbemouret.en.xml", "new.abbemouret.fr.xml", "abbemouret.align.en-fr.xml"),\
              #("confessions.en.xml", "new.confessions.fr.xml", "confessions.align.en-fr.xml"),\
              ("mohicans.en.xml", "new.mohicans.fr.xml", "mohicans.align.en-fr.xml"),\
              #("travailleurs.en.xml", "new.travailleurs.fr.xml", "travailleurs.align.en-fr.xml"),\
              ("candide.en.xml", "new.candide.fr.xml", "candide.align.en-fr.xml"), \
              ("alice.en.xml", "new.alice.fr.xml", "alice.align.en-fr.xml"), \
              #("voyage.en.xml", "new.voyage.fr.xml", "voyage.align.en-fr.xml"), \
              #("20000milles.en.xml", "new.20000milles.fr.xml", "20000milles.align.en-fr.xml"),\
              ("emma.en.xml", "new.emma.fr.xml", "emma.align.en-fr.xml"), \
              ("swann.en.xml", "new.swann.fr.xml", "swann.align.en-fr.xml"),\
              ("janeeyre.en.xml", "new.janeeyre.fr.xml", "janeeyre.align.en-fr.xml"),\
            ]
  obs1D = []
  y1D = []
  allObs = {}
  allY = {}
  allGraph = {}
  #ttable1 = readTTable("/people/yong/TransRead/LinguisticIssues/src/exp12/ttable1.txt")
  #ttable2 = readTTable("/people/yong/TransRead/LinguisticIssues/src/exp12/ttable2.txt")

  dictWordCodeTrg, frequentTrg = word2code("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.fr.vcb")
  dictWordCodeSrc, frequentSrc = word2code("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.en.vcb")
  #ttable1 = code2prob("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.mgiza/fr2en.t1.5", dictWordCodeTrg, dictWordCodeSrc)
  #ttable2 = code2prob("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.mgiza/en2fr.t1.5", dictWordCodeSrc, dictWordCodeTrg)
  ttable1 = code2prob("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.mgiza/fr2en.t3.final", dictWordCodeTrg, dictWordCodeSrc)
  ttable2 = code2prob("/people/yong/TransRead/crf2d/Europarl/ep_v7_low.mgiza/en2fr.t3.final", dictWordCodeSrc, dictWordCodeTrg)

  now = datetime.datetime.now()
  filename = u"{0}{1}_{2}H{3}.txt".format(now.strftime("%d"), now.strftime("%b"), now.strftime("%H"), now.strftime("%M"))
  record = open(u"Experiments/{0}".format(filename), "w")
  record.write(u"Experiment starts on {0} {1}, at {2} H {3}\n".format(now.strftime("%b"), now.strftime("%d"), now.strftime("%H"), now.strftime("%M")).encode("utf-8"))

  """
  ##########  Test, to see feature values #################
  name = aligned[8][0].split(".")[0]
  xcesSrc = xces.xcesTextFile(dataDir + aligned[8][0])
  xcesTrg = xces.xcesTextFile(dataDir + aligned[8][1])
  xcesAlign = xces.xcesAlignFile(dataDir + aligned[8][2])
  label = align2label(xcesSrc, xcesTrg, xcesAlign)
  for indSrc, indTrg in sorted(label) :
    if label[indSrc, indTrg] == 2 :
      print xcesSrc.dictInd2ID[indSrc], xcesTrg.dictInd2ID[indTrg]
  exit()
  obs = text2obs(xcesSrc, xcesTrg,  xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys(), ttable1, ttable2, frequentSrc, frequentTrg, Y)
  g = graph.Graph(xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys())
  pred = {}
  localBel = {}
  logLocalPot = {}
  for x in range(xcesSrc.numSent) :
    for y in range(xcesTrg.numSent) :
      pred[x, y] = 1  
      localBel[x, y] = [0.5, 0.5]
      logLocalPot[x, y] = [0.5, 0.5]
  printInfo(xcesSrc, xcesTrg, name, label, obs, pred, logLocalPot, localBel, record, writeRecord=False) 
  exit()
  """

  record.write(u"Training set :\n".encode("utf-8"))
  for ind, a in enumerate(aligned[0:-3]) :
    print a[0].split(".")[0]
    record.write(u"{0}\n".format(a[0].split(".")[0]).encode("utf-8"))
    xcesSrc = xces.xcesTextFile(dataDir + a[0])
    xcesTrg = xces.xcesTextFile(dataDir + a[1])
    xcesAlign = xces.xcesAlignFile(dataDir + a[2])
    allY[ind] = align2label(xcesSrc, xcesTrg, xcesAlign)
    allObs[ind] = text2obs(xcesSrc, xcesTrg, xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys(),ttable1, ttable2, Y=allY[ind])
    allGraph[ind] = graph.Graph(xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys())
    for i in sorted(allObs[ind]) :
      obs1D.append(allObs[ind][i])
      y1D.append(allY[ind][i])
  #obs1D = np.array(obs1D)
  #y1D = np.array(y1D)
  #logreg = linear_model.LogisticRegression(C=1e5)
  #logreg.fit(obs1D, y1D)
  #print logreg.coef_
  #print len(allObs[0][0,0])
  #w = np.ndarray.flatten(logreg.coef_)
  #crf = graph.CRF(len(allObs[0][0,0]), localWeights=w, alpha=10)
  #crf.train(allGraph, allObs, allY, method = 'bfgs')
  #record.write(u"\n\nCRF parameters : \n".encode("utf-8"))
  #record.write(stringCRFWeights(crf))
  #record.write(u"=====================================\n".encode("utf-8"))
  #crf.writeModel(argv[1])
  crf=graph.CRF()
  crf.readModel("hihi.txt")  
  ################ Train set ##############################
  truePos = 0.0 # True positive predicted labels
  positive = 0.0 # Total count of true labels
  falsePos = 0.0 # False positives
  falseNeg = 0.0 # False negative

  record.write(u"\n\nOn training : \n".encode("utf-8"))
  for ind in range(len(aligned[0:-3])) :
    Y = allY[ind]
    print Y
    obs = allObs[ind]
    g = allGraph[ind]
    pred, localBel = crf.prediction(g, obs)
    for (x, y) in sorted(pred) :
      if Y[x, y] == 2 and pred[x, y] == 1 :
        positive += 1
        falseNeg += 1
      if Y[x, y] == 2 and pred[x, y] == 2 :
        truePos += 1
        positive += 1
      if Y[x, y] == 1 and pred[x, y] == 2 :
        falsePos += 1
  record.write(u"\n\nFalse positive : {0}\nTrue positive : {1}\nFalse negative : {2}\nPositive : {3}\nTrue positive / positive :  {4:.3f}\n\n".format(falsePos, truePos, falseNeg, positive, truePos / positive).encode("utf-8"))
  print "\n\n"
  print "False positive : {0}\nTrue positive : {1}\nFalse negative : {2}\nPositive : {3}".format(falsePos, truePos, falseNeg,     positive)
  print "True positive / positive : {0:.3f}".format(truePos / positive)
  print "\n\n"
  record.write(u"=====================================\n".encode("utf-8"))
  exit()

  ################  Test set ##############################
  truePos = 0.0 # True positive predicted labels
  positive = 0.0 # Total count of true labels
  falsePos = 0.0 # False positives
  falseNeg = 0.0 # False negative

  record.write(u"\n\nDoing test : \n".encode("utf-8"))

  name = aligned[-3][0].split(".")[0]
  xcesSrc = xces.xcesTextFile(dataDir + aligned[-3][0])
  xcesTrg = xces.xcesTextFile(dataDir + aligned[-3][1])
  xcesAlign = xces.xcesAlignFile(dataDir + aligned[-3][2])
  label = align2label(xcesSrc, xcesTrg, xcesAlign)
  obs = text2obs(xcesSrc, xcesTrg,  xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys(), ttable1, ttable2, Y=label)
  g = graph.Graph(xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys())
  pred, localBel = crf.prediction(g, obs)
  logLocalPot = graph.makeLocalPot(g, crf.localWeights, obs)
  tp, pos, fp, fn = printInfo(xcesSrc, xcesTrg, name, label, obs, pred, logLocalPot, localBel, record, writeRecord=True)
  truePos += tp
  positive += pos
  falsePos += fp
  falseNeg += fn
  #label2links(pred, g, xcesSrc, xcesTrg)

  name = aligned[-2][0].split(".")[0]
  xcesSrc = xces.xcesTextFile(dataDir + aligned[-2][0])
  xcesTrg = xces.xcesTextFile(dataDir + aligned[-2][1])
  xcesAlign = xces.xcesAlignFile(dataDir + aligned[-2][2])
  label = align2label(xcesSrc, xcesTrg, xcesAlign)
  obs = text2obs(xcesSrc, xcesTrg, xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys(), ttable1, ttable2, Y=label)
  g = graph.Graph(xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys())
  pred, localBel = crf.prediction(g, obs)
  logLocalPot = graph.makeLocalPot(g, crf.localWeights, obs)
  tp, pos, fp, fn = printInfo(xcesSrc, xcesTrg, name, label, obs, pred, logLocalPot, localBel, record, writeRecord=True)
  truePos += tp
  positive += pos
  falsePos += fp
  falseNeg += fn
  #label2links(pred, g, xcesSrc, xcesTrg)
  
  name = aligned[-1][0].split(".")[0]
  xcesSrc = xces.xcesTextFile(dataDir + aligned[-1][0])
  xcesTrg = xces.xcesTextFile(dataDir + aligned[-1][1])
  xcesAlign = xces.xcesAlignFile(dataDir + aligned[-1][2])
  label = align2label(xcesSrc, xcesTrg, xcesAlign)
  obs = text2obs(xcesSrc, xcesTrg, xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys(), ttable1, ttable2, Y=label)
  g = graph.Graph(xcesSrc.dictInd2ID.keys(), xcesTrg.dictInd2ID.keys())
  pred, localBel = crf.prediction(g, obs)
  logLocalPot = graph.makeLocalPot(g, crf.localWeights, obs)
  tp, pos, fp, fn = printInfo(xcesSrc, xcesTrg, name, label, obs, pred, logLocalPot, localBel, record, writeRecord=True)
  truePos += tp
  positive += pos
  falsePos += fp
  falseNeg += fn
  #label2links(pred, g, xcesSrc, xcesTrg)

  record.write(u"\n\nFalse positive : {0}\nTrue positive : {1}\nFalse negative : {2}\nPositive : {3}\nTrue positive / positive : {4:.3f}\n\n".format(falsePos, truePos, falseNeg, positive, truePos / positive).encode("utf-8"))
  print "\n\n"
  print "False positive: {0}\nTrue positive: {1}\nFalse negative: {2}\nPositive: {3}".format(falsePos, truePos, falseNeg, positive)
  print "True positive / positive : {0:.3f}".format(truePos / positive)

  endTime = datetime.datetime.now()
  record.write(u"Experiment ends on {0} {1}, at {2} H {3}.\n".format(endTime.strftime("%b"), endTime.strftime("%d"), endTime.strftime("%H"), endTime.strftime("%M")).encode("utf-8"))
