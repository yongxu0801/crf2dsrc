# -*- coding: utf-8 -*-

"""
This script contains a class of 2D grid graphical 
model, and a class of CRF implementation. The inference
is done using loopy belief probagation. The training
is done using maximum likelihood estimation, with 
optimization routines (BFGS, conjugate gradient) 
provided by Python Scipy package.  
"""

import numpy as np
from codecs import open
from copy import deepcopy
import scipy.optimize as opt
from itertools import product, izip_longest
import multiprocessing as mp
import heapq
import time

def expDict(d) :
  """
  For every (key, value) pair in a dictionary, replace every 
  value by exp(value).
  """
  for x in d : d[x] = np.exp(d[x])
  return d

def logMaximumMatrixVectorMult(logM, logN) :
  """
  The same with matrix vector multiplication, except the sum operator is
  replaced by the max operator. This is used in the max-product
  version of LBP.
  logN should be a row array in numpy.
  """
  mrow, mcol = logM.shape
  res = []
  for i in xrange(mrow) :
      res.append( np.max(logM[i, :] + logN) ) 
  res = np.array(res)
  return res

def logSum(logCollection) :
  """
  For a collection of positive numbers a, we have b = log(sum(a)).
  Now, if instead we have logA (log of a), this method computes b.
  logCollection should be a numpy array.
  """
  m = np.max(logCollection)
  return m + np.log( np.sum([np.exp(x - m) for x in np.nditer(logCollection)]) )

def logMatrixVecMult(logM, logN) :
  """
  For a (positive) matrix m and a vector n, we have c = log(m * n).
  Now, if we have instead logM (log of m) and logN (log of n), this method computes c 
  """
  nrow, ncol = logM.shape
  res = []
  for i in xrange(nrow) :
    tmp = []
    for j in xrange(ncol) :
      tmp.append(logM[i, j] + logN[j])
    res.append(logSum(tmp))
  return np.array(res)

def logMatrixMult(logM, logN) :
  """
  For positive matrices m and n, we have c = log(m * n)
  Now, if we have logM (log of m) and logN (log of N), this method computes c
  """
  mrow, mcol = logM.shape
  nrow, ncol = logN.shape
  res = np.array([])
  for j in xrange(ncol) :
    res = np.hstack((res, logMatrixVecMult(logM, logN[:, j])))
  return np.transpose(res.reshape(ncol, mrow))

def logNormalise(logCollection) :
  """
  For a positive array [a, b], the normalisation process gives [a, b] / (a+b)
  Now, if we are given [log(a), log(b)], this methods computes log( [a, b] / (a + b) )
  """
  logS = logSum(logCollection)
  return logCollection - logS

class CRF :
  
  """
  CRF implementation. One can initialize the weights by passing parameters, 
  if not, they will be initilized randomly. One can also specify the L2
  regularization weight alpha (the weight to put on ||w^2|| regularizer term,
  alpha=1/sigma^2 ).
  """

  def __init__(self, numFeatures=0, localWeights=None, pairPot=None, alpha=0.0) :
    self.numFeatures = numFeatures
    self.alpha = alpha
    if localWeights is None :
      self.localWeights = 0.1 * np.random.normal(size=self.numFeatures)
    else :
      self.localWeights = np.copy(localWeights)
    if not pairPot is None :
      self.pairPot = np.copy(pairPot)
    else :
      pot = 0.1 * np.random.random((2, 2))
      pot = pot + np.transpose(pot)
      self.pairPot = np.copy(pot) # PairPot is a symmetric 2 * 2 matrix 

    # For the test in align.py
    if numFeatures == 10 :
      self.localWeights = np.array([-1.46823613,  -0.13177642, -55.79234173,  16.52500101,  59.96103741, 5.04630483, -11.72493703, 11.64270223, -19.67927268,  -0.18505353])
      self.pairPot = np.exp(np.array([[-5.58359837,  -6.28823435],  [-6.28823435,  -4.84594061]]))
    # For the test in this file
    if numFeatures == 2 :
      self.localWeights = np.array([0.0864,   0.0094]) 
      self.pairPot = np.array([[0.1906, 0.1658], [0.1658, 0.1196]])

  def pack(self) :
    """ Pack all CRF weights into a row array (as required by optimization routines) """
    return np.hstack((np.hstack(self.localWeights), np.log(np.hstack(self.pairPot))))

  def unpack(self, newPara) :
    """ Store the updated weights into the model."""
    self.localWeights = np.copy(newPara[0:-4])
    self.pairPot = np.copy(np.exp(newPara[-4:].reshape(2,2)))
    #  Become 
    #  (S0 -> S0), (S0 -> S1)
    #  (S1 -> S0), (S1 -> S1)
    return

  def train(self, allGraph, allObs, allY, method=u"bfgs") :
    """ 
    Train the CRF model using optimization routines in the Scipy package.
    The method parameter can take the value "BFGS" or "CG" (for conjugate gradient). 
    """
    weights = np.copy(self.pack()) # local weights and the log of pairwise potentials
    # pairwise potential : [(S0 -> S0), (S0 -> S1), (S1 -> S0), (S1 -> S1)]
    method = method.strip().lower()
    if method != u"bfgs" and method != u"cg" :
      print u"The optimization method {0} is not supported.\n Using BFGS ...".format(method).encode("utf-8")
      wopt = opt.fmin_bfgs(errorCRF, weights, fprime = gradientCRF, args=(allGraph, allObs, allY, self.numFeatures, self.alpha))
    elif method == u'bfgs' :
      print u"The optimization method is BFGS.".encode("utf-8")
      wopt = opt.fmin_bfgs(errorCRF, weights, fprime = gradientCRF, args=(allGraph, allObs, allY, self.numFeatures, self.alpha))
    elif method == u'cg' :
      print u"The optimization method is CG.".encode("utf-8")
      wopt = opt.fmin_cg(errorCRF, weights, fprime = gradientCRF, args=(allGraph, allObs, allY, self.numFeatures, self.alpha))
    print u"Optimization finished.".encode("utf-8")
    self.unpack(wopt)
    return wopt

  def prediction(self, graph, obs) :
    """
    Perform the prediction task using the actual weights stored in the model. 
    The loopy belief probagation algorithm is used for the inference. If it is
    launched with the maxProduct flag, then it will use the max-product message passing;
    otherwise, the sum-product message passing is used.
    """
    print "Doing prediction : "
    pred = {}
    logLocalPot = makeLocalPot(graph, self.localWeights, obs)
    localBel, pairBel = lbp(graph, logLocalPot, np.log(self.pairPot), computeLogZ=False, maxProduct=True)
    for (row, col) in graph.nodes :
      if localBel[(row, col)][0] >= localBel[(row, col)][1] :
        pred[(row, col)] = 1
      else : pred[(row, col)] = 2
    print "Prediction finished. "
    return pred, localBel

  def writeModel(self, fn) :
    """ Write model weights into a file."""
    w = open(fn, "w")
    w.write(u"Local weights\n".encode("utf-8"))
    for lw in np.hstack(self.localWeights) :
      w.write(u"{0}\n".format(lw).encode("utf-8"))
    w.write(u"Pairwise potential\n".encode("utf-8"))
    for pp in np.hstack(self.pairPot) :
      w.write(u"{0}\n".format(pp).encode("utf-8"))


  def readModel(self, fn) :
    """ Read model weights from a file."""
    lw = []
    pp = []
    readingW = False
    readingP = False
    for line in open(fn, "r") :
      line = line.strip().decode("utf-8").lower()
      if not readingW and line == u"local weights" :
        readingW = True
        continue
      if readingW and line != u"pairwise potential":
        lw.append(float(line))
        continue
      if readingW and line == u"pairwise potential" :
        readingW = False
        readingP = True
        continue
      if readingP :
        pp.append(float(line))

    self.localWeights = np.array(lw)
    self.pairPot = np.array(pp).reshape(2,2)
    self.numFeatures = len(self.localWeights)

class Graph :
  """
  A 2D grid graphical model. Need to specify the 
  list of source indices and the list of target indices.
  """
  def __init__(self, srcInds, trgInds) :
    self.srcInds = sorted(srcInds)
    self.trgInds = sorted(trgInds)
    self.nodes = sorted(list(product(self.srcInds, self.trgInds)))

  def getNeighbours(self, indSrc, indTrg) :
    """ 
    Specify the connectivity relation in the model. 
    The connections are :

    1 - 2 - 3
    |   |   |
    4 - 5 - 6
    |   |   |
    7 - 8 - 9

    Thus, the node 1 is connected to 2 and 4, 
    the node 5 is connected to 2, 4, 6, 8.  
    """
    nbors = []
    if (indSrc - 1, indTrg) in self.nodes : # upper 
      nbors.append((indSrc - 1, indTrg))
    if (indSrc, indTrg + 1) in self.nodes  :  # right
      nbors.append((indSrc, indTrg + 1))
    if (indSrc + 1, indTrg) in self.nodes : # down
      nbors.append((indSrc + 1, indTrg))
    if (indSrc, indTrg - 1) in self.nodes : # left
      nbors.append((indSrc, indTrg - 1))

    return nbors


def isAfter(indSrc, indTrg, nborIndSrc, nborIndTrg) :
  """
  Test whether the neighbor (coordinate (nborIndSrc, nborIndTrg)) is "after" 
  the current node (coordinate (indSrc, indTrg)) in the document order.
  """
  return nborIndSrc + nborIndTrg > indSrc + indTrg

def approxeq(a, b, tol=1e-2) :
  """ Test whether a and b are approximately pairwisely equivalent."""
  return (not np.any(np.abs(a - b) > tol))

def lbp(graph, log_localPot, log_pairPot, numIter=1000, tol=1e-3, computeLogZ=True, maxProduct=False) :
  """
  Loopy belief probagation inference algorithm. 
  graph : the graphical structure of the model;
  log_localPot : log of local evidence on each node;
  log_pairPot : log of pairwise potential, shared everywhere (can be changed);
  numIter : in case of non convergence, the maximum number of message-passing iterations;
  tol : tolerance of difference, used to test convergence
  computeLogZ : whether to compute the log partition function 
  maxProduct : whether to use max-product message passing; if not, sum-product is used.
  """

  # One important lesson : should not use = to copy numpy array, use np.copy instead !
  log_oldBelief = {}
  log_newBelief = {}
  log_msg = {}

  # Initiasation of the messages
  for (row, col) in graph.nodes :
    log_oldBelief[(row, col)] = np.copy(log_localPot[(row, col)])
    nbs = graph.getNeighbours(row, col)
    for (nborRow, nborCol) in nbs :
      log_msg[(row, col), (nborRow, nborCol)] = np.log(np.array([0.5, 0.5]))

  iteration = 1
  converged = False
  while (not converged) and (iteration < numIter) :
    for (row, col) in sorted(graph.nodes) :
      nbs = graph.getNeighbours(row, col)
      for (nborRow, nborCol) in sorted(nbs) :
        temp = np.copy(log_localPot[(row, col)])
        for (otherNborRow, otherNborCol) in nbs :
          if (nborRow, nborCol) == (otherNborRow, otherNborCol) : continue
          temp += log_msg[(otherNborRow, otherNborCol), (row, col)] 
        if isAfter(row, col, nborRow, nborCol) :
          if not maxProduct :
            log_msg[(row, col),(nborRow, nborCol)] = logMatrixVecMult(np.transpose(log_pairPot), temp)
          else : # This is max-product
            log_msg[(row, col),(nborRow, nborCol)] = logMaximumMatrixVectorMult(np.transpose(log_pairPot), temp)
        else :
          if not maxProduct :
            log_msg[(row, col),(nborRow, nborCol)] = logMatrixVecMult(log_pairPot, temp)
          else :
            log_msg[(row, col),(nborRow, nborCol)] = logMaximumMatrixVectorMult(log_pairPot, temp)
        log_msg[(row, col), (nborRow, nborCol)] = logNormalise(log_msg[(row, col), (nborRow, nborCol)])

    for row,col in graph.nodes :
      temp = np.copy(log_localPot[(row, col)])
      for (nborRow, nborCol) in graph.getNeighbours(row, col) :
        temp += log_msg[(nborRow, nborCol), (row, col)]
      log_newBelief[(row, col)] = logNormalise(temp)

    converged = True
    for row,col in graph.nodes:
      if np.any(np.abs(np.exp(log_newBelief[row, col]) - np.exp(log_oldBelief[row, col])) >= tol) :
        converged = False
    iteration += 1
    log_oldBelief = deepcopy(log_newBelief)

  if converged :
    print(u"The lbp converged in {0} iterations\n".format(iteration-1))
  if not converged :
    print(u"The lbp stopped after {0} iterations, it has not converged.\n".format(numIter))

  # Now compute edge beliefs
  log_edgeBelief = {}
  for row,col in graph.nodes :
    for (nborRow, nborCol) in graph.getNeighbours(row, col) :
      if not isAfter(row, col, nborRow, nborCol) : continue
      kernel = log_pairPot
      log_beli = log_newBelief[(row, col)] - log_msg[(nborRow, nborCol), (row, col)]
      log_belj = log_newBelief[(nborRow, nborCol)] - log_msg[(row, col), (nborRow, nborCol)]
      temp = logMatrixMult(log_beli.reshape(2,1), log_belj.reshape(1,2))
      temp += kernel
      log_edgeBelief[(row, col), (nborRow, nborCol)] = logNormalise(temp)
  if not computeLogZ : return (expDict(log_newBelief), expDict(log_edgeBelief))

  # Now compute the approximated log partition function .
  E1 = 0.0
  E2 = 0.0
  H1 = 0.0
  H2 = 0.0
  for row, col in graph.nodes :
    nbors = graph.getNeighbours(row, col)
    b = np.copy(log_newBelief[(row, col)])
    H1 += (len(nbors) - 1) * (np.sum(np.exp(b) * b))
    E1 -= np.sum(np.exp(b) * log_localPot[(row, col)])
    for nborRow, nborCol in nbors :
      if not isAfter(row, col, nborRow, nborCol) : continue
      b = np.copy(log_edgeBelief[(row, col), (nborRow, nborCol)])
      H2 -= np.sum(np.exp(b) * b)
      E2 -= np.sum(np.exp(b) * log_pairPot)
  freeEnergy = (E1 + E2) - (H1 + H2)
  logZ = -freeEnergy
  return (expDict(log_newBelief), expDict(log_edgeBelief), logZ)

def localFeatureExpectation(feature, belief) :
  """ Compute the expectation of features the given distribution. """
  expectation = []
  for prob in belief :
    for val in feature :
      expectation.append(val * prob)
  return np.array(expectation)

def makeLocalPot(graph, weight, obs) :
  """ Compute teh local evidence on each node. """
  logLocalPot = {}
  for row, col in graph.nodes :
      temp = 0.0
      for ind, val in enumerate(obs[(row, col)]) :
        temp += weight[ind] * val
      logLocalPot[(row, col)]= np.array([0.0, temp])
      logLocalPot[(row, col)] = logNormalise(logLocalPot[(row, col)])
  return logLocalPot

def instanceError(weights, graph, obs, y, numFeatures, alpha) :
  """
  The loss on one tranining instance. The loss is the negative 
  likelihood. Thus, for one instance (x, y), 
  loss = - \sum_{t} W' * F(y_t, y_{t-1}, x) + log(Z(x))
  """
  localWeights = weights[0:numFeatures]
  logLocalPot = makeLocalPot(graph, localWeights, obs)
  logPairPot = weights[numFeatures:].reshape((2,2))
  localBel, pairBel, logZ = lbp(graph, logLocalPot, logPairPot, computeLogZ=True)
  eI = 0.0
  eE = 0.0
  for row, col in graph.nodes :
    feature = obs[(row, col)] # Local feature vector
    if y[row, col] == 1 : trueBel = [1, 0]
    if y[row, col] == 2 : trueBel = [0, 1]
    # clamped one state
    trueBel = trueBel[1:] # trueBel == [0] or trueBel == [1]
    empiricalExp = localFeatureExpectation(feature, trueBel)
    errorNode = np.dot(np.hstack(localWeights), empiricalExp)
    eI += errorNode

    for (nborRow, nborCol) in graph.getNeighbours(row, col) :
      if not isAfter(row, col, nborRow, nborCol) : continue
      trueBel = np.zeros((2, 2))
      trueBel[y[row, col]-1, y[nborRow, nborCol]-1] = 1
      pairWeights = logPairPot
      errorEdge = np.dot(np.hstack(pairWeights), np.hstack(trueBel))
      assert(approxeq(pairWeights[y[row, col]-1, y[nborRow, nborCol]-1], errorEdge))
      eE += errorEdge

  return -(eI + eE - logZ)

def errorCRF(weights, allGraph, allObs, allY, numFeatures, alpha = 0.0) :
  """
  Compute the total error of the model on all the instances.
  """
  a = time.time()
  pool = mp.Pool(processes=8)
  res = [pool.apply_async(instanceError, args=(weights, allGraph[i], allObs[i], allY[i], numFeatures, alpha,)) for i in allY.     keys()]
  output = [p.get() for p in res]
  # Only regularize the pairwise potentials
  #error = sum(output) + alpha * 0.5 * np.dot(weights[-4:], np.transpose(weights[-4:]))
  error = sum(output) + alpha * 0.5 * np.dot(weights, np.transpose(weights)) + 10 * alpha * 0.5 * np.dot(weights[-4:], np.transpose(weights[-4:]))
  pool.close()
  pool.join()
  return error

  """
  b = time.time()
  listInstanceError = []
  for instance in sorted(allY.keys()) :
    y = allY[instance]
    obs = allObs[instance]
    graph = allGraph[instance]
    error = instanceError(weights, graph, obs, y, numFeatures, alpha) 
    listInstanceError.append(error)
  error =  sum(listInstanceError) + alpha * 0.5 * np.dot(weights[-4:], np.transpose(weights[-4:]))
  return error
  """

def instanceGradient(weights, graph, obs, y, numFeatures, alpha) :
  """
  The gradient of the error function, contributed by one instance.
  """
  localWeights = weights[0:numFeatures]
  logLocalPot = makeLocalPot(graph, localWeights, obs)
  logPairPot = weights[numFeatures:].reshape((2,2))
  localBel, pairBel = lbp(graph, logLocalPot, logPairPot, computeLogZ=0)
  gI = np.zeros(numFeatures)
  gE = np.zeros(4)
  for row, col in graph.nodes :
    trueBel = [0, 0]
    feature = obs[(row, col)]
    if y[row, col] == 1 : trueBel = [1, 0]
    if y[row, col] == 2 : trueBel = [0, 1]
    trueBel = trueBel[1:]
    localBel[row, col] = localBel[row, col][1:]
    empiricalExp = localFeatureExpectation(feature, trueBel)
    modelExp = localFeatureExpectation(feature, localBel[row, col])
    gI += empiricalExp - modelExp

    for (nborRow, nborCol) in graph.getNeighbours(row, col) :
      if not isAfter(row, col, nborRow, nborCol) : continue
      trueBel = np.zeros((2, 2))
      trueBel[y[row, col]-1, y[nborRow, nborCol]-1] = 1
      bel = pairBel[(row, col), (nborRow, nborCol)]
      gE += np.hstack(trueBel) - np.hstack(bel)
  return -np.hstack((gI, gE))

def gradientCRF(weights, allGraph, allObs, allY, numFeatures, alpha = 0.0) :
  """
  The gradient of the loss function.
  """
  a = time.time()
  pool = mp.Pool(processes=8)
  res = [pool.apply_async(instanceGradient, args=(weights, allGraph[i], allObs[i], allY[i], numFeatures, alpha,)) for i in allY.  keys()]
  output = [p.get() for p in res]
  grad = np.zeros((1, numFeatures + 4))
  for x in output : grad += x
  tmp = np.copy(weights)
  tmp[0:-4] = 0.0
  g = np.ndarray.flatten(grad) + alpha * weights + 10 * alpha * tmp
  #g = np.ndarray.flatten(grad) + alpha * weights
  pool.close()
  pool.join()
  return g

  """
  b = time.time()
  grad = np.zeros((1, numFeatures + 4))  
  # For each training instance
  for instance in sorted(allY.keys()) :
    y = allY[instance]
    obs = allObs[instance] 
    graph = allGraph[instance]
    g = instanceGradient(weights, graph, obs, y, numFeatures, alpha) 
    grad += g
  tmp = np.copy(weights)
  tmp[0:-4] = 0.0
  #g = np.ndarray.flatten(grad) + alpha * weights
  g = np.ndarray.flatten(grad) + alpha * tmp
  return g
  """

if __name__ == "__main__" :
  """
  Simple test code.
  """
  allGraph = {1 : Graph([0, 1], [0, 1]), 2 : Graph([0, 1], [0, 1])}
  allObs = {1 : {(0, 0) : [3, 1], (0, 1) : [2, 1], (1, 0) : [0.5, 1], (1, 1) : [0.3, 1]},
          2 : {(0, 0) : [1.7, 1], (0, 1) : [1.2, 1], (1, 0) : [0.2, 1], (1, 1) : [0.1, 1]}}

  allY = {1 : {(0, 0) : 1, (0, 1) : 1, (1, 0) : 2, (1, 1) : 2},
        2 : {(0, 0) : 1, (0, 1) : 1, (1, 0) : 2, (1, 1) : 2}}
  testCRF = CRF(2)
  testCRF.train(allGraph, allObs, allY, "BFGS")
  testGraph = Graph([0, 1, 2], [0, 1])
  testObs = {(0, 0) : [1.5, 1], (0, 1) : [0.8, 1], (1, 0) : [0.4, 1], (1, 1) : [2, 1], (2, 0) : [0.1, 1], (2, 1) : [5, 1]}
  pred, localBel = testCRF.prediction(testGraph, testObs)
  for x in sorted(pred) :
    print x, localBel[x], testObs[x], pred[x], pred[x] == (1 if testObs[x][0] > 1 else 2)
  testCRF.writeModel("testCRF.model")

