# -*- coding: utf-8 -*-

"""
Features for the sentence pair parallel/non-parallel 
classification task.

We assume that we have a word assciation table at hand,
that can be, for example, an IBM-1 translation table.
"""

from sys import argv
from math import log, exp
from string import punctuation

def discretize(real, dis=10) :
  """
  For a number in [0, 1], compute a discretized 
  representation of dis entries, with only one being
  1 and the others being all 0. The number of intervals is 
  specified by the parameter dis.
  """
  try :
    fea = [0] * dis
    i = int(real * dis)
    if i >= dis : i = dis-1
    fea[i] = 1
    return fea
  except : 
    print real
    exit()
  
  
def fIndex(ind1, ind2, num1, num2) :
  """
  A feature capturing the relative position difference
  on the two sides. 10 features.
  """
  fea = discretize(abs(float(ind1)/num1 - float(ind2)/num2))
  return fea

def fLength(sent1, sent2) :
  """
  Discretized length ratio of the two sides (diff / length_src, diff / length_trg).
  Lengthes are measured in terms of characters, except for very short ones,
  for which lengthes are measured as number of words. 20 features.
  """
  if len(sent1.split()) < 5 or len(sent2.split()) < 5 :
    length1 = len(sent1.split())
    length2 = len(sent2.split())
  else :
    length1 = len(sent1)
    length2 = len(sent2)
  length_diff = float(abs(length1 - length2))
  fea = discretize(length_diff / length1) + discretize(length_diff / length2)
  return fea
 
def fCopy(sent_src, sent_trg, label=1) :
  """
  Ratio between number of identical word pairs and total number
  of words. One ratio for one side. This does not count punctuations.
  20 features.
  """
  list_src = sent_src.lower().split()
  list_trg = sent_trg.lower().split()
  count_copy = 0

  dict_src = {}
  for word in list_src:
    if word in punctuation : continue
    dict_src[word] = dict_src.get(word, 0) + 1

  dict_trg = {}
  for word in list_trg:
    if word in punctuation : continue
    dict_trg[word] = dict_trg.get(word, 0) + 1

  for word in dict_src:
    if word in dict_trg :
      min_num = min(dict_src[word], dict_trg[word])
      count_copy += min_num

  ratioS = float(count_copy) / len(list_src)
  ratioT = float(count_copy) / len(list_trg)
  fea = discretize(ratioS) + discretize(ratioT)
  return fea
  
def fCognate(sent_src, sent_trg, label=1) :
  """
  Ratio between number of cognates and total number of words.
  A pair of words constitue a pair of cognate if they 
  share the first 4 characters. One ratio for one side. 20 features.
  """
  list_src = sent_src.lower().split()
  list_trg = sent_trg.lower().split()
  count_cog = 0

  dict_src_cog = {}
  for word in list_src:
    if len(word) < 4 : continue
    cog = word[0:4]
    dict_src_cog[cog] = dict_src_cog.get(cog, 0) + 1

  dict_trg_cog = {}
  for word in list_trg:
    if len(word) < 4 : continue
    cog = word[0:4]
    dict_trg_cog[cog] = dict_trg_cog.get(cog, 0) + 1 

  for cog in dict_src_cog :
    if cog in dict_trg_cog :
      count_cog += min(dict_src_cog[cog], dict_trg_cog[cog]) 

  ratioS = float(count_cog) / len(list_src)
  ratioT = float(count_cog) / len(list_trg)
  fea = discretize(ratioS) + discretize(ratioT)
  return fea

def alignment(source, target, ttable, threshold) :
  """
  Construct the IBM 1 alignment based on the two translation tables.
  Source E {0...I}, target F {1...J}. Return a dictionary
  such that dictAlign[j] = argmax_{i \in {0...I}} t(F_j | E_i)
  """
  wordSrc = source.lower().split()
  wordSrc.insert(0, u"NULL")
  wordTrg = target.lower().split()

  dictAlignment = {}
  for index, trg in enumerate(wordTrg) :
    bestScore = ttable[u"NULL"].get(trg, threshold)
    bestPos = 0
    for ind, src in enumerate(wordSrc) :
      if ttable.get(src, {}).get(trg, threshold) > bestScore :
        bestScore = ttable.get(src, {}).get(trg, threshold)
        bestPos = ind
    dictAlignment[index+1] = bestPos

  return dictAlignment

def fLexicalProb(sent1, sent2, ttable1, ttable2, frequentSrc, frequentTrg, label=1) :
  """
  Lexical translation probability score, computed with the IBM-1 translaiton table. 
  ttable1 : target to source translation table.
  ttable2 : source to target translation table.
  T->S: score(S, T) = \frac{1}{J}\sum_{j=1}^{J}\log(\frac{1}{I} * \sum_{i=1}^{I}p(s_j|t_i))
  S->T is defined similarly.
  We need two IBM-1 tables, in different directions.
  """
  src = sent1.lower().split()
  source = []
  if len(src) < 5 : source = src
  else :
    for ws in src : 
      if (ws not in punctuation) and (ws not in frequentSrc): 
        source.append(ws)
  trg = sent2.lower().split()
  target = []
  if len(trg) < 5 : target = trg
  else :
    for wt in trg :
      if (wt not in punctuation) and (wt not in frequentTrg):
        target.append(wt) 

  if len(source) == 0 or len(target) == 0 : 
    source = src
    target = trg

  ts = 0.0
  for sw in source :
    #ts += log(sum([ttable1.get(tw, {}).get(sw, 1e-6) for tw in target]) / lengthT)
    cand = []
    for tw in target :
      if tw == sw : score = 1.0
      elif tw[0:4] == sw[0:4] : score = 0.5
      else : score = ttable1.get(tw, {}).get(sw, 1e-6)
      cand.append(score)
    cand.sort()
    if len(cand) > 1 : cand = cand[-1:]
    ts += log(sum(cand) / float(len(cand)))
  ts /= (len(source)) 

  st = 0.0
  for tw in target :
    #st += log(sum([ttable2.get(sw, {}).get(tw, 1e-6) for sw in source]) / lengthS)
    cand = []
    for sw in source :
      if sw == tw : score = 1.0
      elif tw[0:4] == sw[0:4] : score = 0.5
      else : score = ttable2.get(sw, {}).get(tw, 1e-6)
      cand.append(score)
    cand.sort()
    if len(cand) > 1 : cand = cand[-1:]
    st += log(sum(cand) /float(len(cand)))
  st /= (len(target))
  
  mean = (ts + st) / 2
  #print sent1.encode("utf-8")
  #print sent2.encode("utf-8")
  #print ts, st, mean
  
  l1 = discretize(abs(ts) / 10.0)
  #l1[-1] = -ts
  l2 = discretize(abs(st) / 10.0)
  #l2[-1] = -st
  l3 = discretize(abs(mean) / 10.0)
  #l3[-1] = - mean
  
  fea = l1 + l2 + l3 
  return fea

def getFingerprint(xcesText, ind) : 
  """
  The fingerprint of a sentence is the set of 
  words that appear in neither the previous nor
  the next sentence. It helps to establish the sentence's
  "identity". 
  """
  sent = xcesText.findSentenceByInd(ind).lower()
  prev = xcesText.findSentenceByInd(ind-1).lower()
  next = xcesText.findSentenceByInd(ind+1).lower()

  fp = set(sent.split()) - set(prev.split()) - set(next.split())
  return fp

     
def fFingerprint(xcesSrc, xcesTrg, indSrc, indTrg, ttable1, ttable2, frequentSrc, frequentTrg, label=1) :
  """
  A word is said to be covered if there is a word on the other side
  with which the lexical translation probability is high. This feature
  compute the ratio of covered fingerprint words. The most frequent
  words of the language of the sentence are excluded from its fingerprint.
  20 features.
  """
  fpSrc = getFingerprint(xcesSrc, indSrc) - set(frequentSrc)
  fpTrg = getFingerprint(xcesTrg, indTrg) - set(frequentTrg)
  if len(fpSrc) == 0 or len(fpTrg) == 0 : 
    return discretize(0.0) + discretize(0.0)

  coveredSrc = 0.0
  for ws in fpSrc :
    for wt in fpTrg :
      if ttable1.get(wt, {}).get(ws, 1e-6) > 1e-2 or ws == wt :
        coveredSrc += 1
        break  
  coveredTrg = 0.0
  for wt in fpTrg :
    for ws in fpSrc :
      if ttable2.get(ws, {}).get(wt, 1e-6) > 1e-2 or ws == wt:
        coveredTrg += 1
        break

  ratioCoveredSrc = coveredSrc / len(fpSrc)
  ratioCoveredTrg = coveredTrg / len(fpTrg)
  fea = discretize(ratioCoveredSrc) + discretize(ratioCoveredTrg)
  return fea

def fShareRareWord(xcesSrc, xcesTrg, sentSrc, sentTrg, label=1) :
  """
  If the two sides share a very rare word (dates, numbers, etc),
  it is possible that they are parallel. This feature captures the
  number of shared rare words (not ratio, which would be 0 in mosts
  cases). 10 features. 
  """
  listWS = sentSrc.lower().split()
  listWT = sentTrg.lower().split()

  matchRareSrc = 0.0
  for ws in listWS :
    if xcesSrc.wordCount[ws] > 3 : continue
    for wt in listWT :
      if wt[0:4] == ws[0:4] :
        matchRareSrc += 1.0 
        break
  matchRareTrg = 0.0
  for wt in listWT :
    if xcesTrg.wordCount[wt] > 3 : continue
    for ws in listWS :
      if ws[0:4] == wt[0:4] :
        matchRareTrg += 1.0 

  l1 = []
  if matchRareSrc >= 3 : l1 =  [0, 0, 0, 0, 1]
  elif matchRareSrc >= 2 : l1 =  [0, 0, 0, 1, 0]
  elif matchRareSrc >= 1 : l1 =  [0, 0, 1, 0, 0]
  elif matchRareSrc > 0 : l1 =  [0, 1, 0, 0, 0]
  elif matchRareSrc == 0 : l1 =  [1, 0, 0, 0, 0]

  l2 = []
  if matchRareTrg >= 3 : l2 =  [0, 0, 0, 0, 1]
  elif matchRareTrg >= 2 : l2 =  [0, 0, 0, 1, 0]
  elif matchRareTrg >= 1 : l2 =  [0, 0, 1, 0, 0]
  elif matchRareTrg > 0 : l2 =  [0, 1, 0, 0, 0]
  elif matchRareTrg == 0 : l2 =  [1, 0, 0, 0, 0]

  fea = l1 + l2 
  return fea

if __name__ == "__main__" :
  pass
