# -*- coding: utf-8 -*-

"""
This script contains some basic classes that deal with
xces format documents.
"""

from sys import argv, path
from os.path import join, exists
import xml.etree.ElementTree as ET
try:
    from collections import OrderedDict
except ImportError:
    # python 2.6 or earlier, use backport
    from ordereddict import OrderedDict
from os import stat
from codecs import open
import HTMLParser as HP
from cgi import escape

h = HP.HTMLParser()

def htmlEscape(text):
  return escape(text, quote=True)

def htmlUnescape(text):
  if text == "''" : text = '"'
  return h.unescape(text)

def indent(elem, level=0):
  i = "\n" + level*"  "
  if len(elem):
    if not elem.text or not elem.text.strip():
      elem.text = i + "  "
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
    for elem in elem:
      indent(elem, level+1)
    if not elem.tail or not elem.tail.strip():
      elem.tail = i
  else:
    if level and (not elem.tail or not elem.tail.strip()):
      elem.tail = i

dictLangCode = {u"English" : u"en", u"French" : u"fr", u"Hungarian" : u"hu",
                u"Spanish" : u"es", u"Italian" : u"it", u"Portuguese" : u"pt",
                u"German" : u"de", u"Esperanto" : u"eo", u"Catalan" : u"ca",
                u"Dutch" : u"nl", u"Finnish" : u"fi", u"Greek" : u"el",
                u"Norwegian" : u"no", u"Russian" : u"ru", u"Swedish" : u"sv"}

class FileError :

  """ The exception to be thrown when a file can not be found or is empty . """

  def __init__(self, fileName):
    print (u"FileError : The file %s cannot be found or it is empty." % (fileName));

class FileTypeError :

  """ The exception to be thrown when the file type is wrong. """

  def __init__(self, fileName, message):
    print (u"FileTypeError %s : %s" % (fileName, message));

def writeXCES(textNode, fileName) :
  """ Write a "text" node (XCES format) into the file fileName """
  with open(fileName, "w") as f :
    f.write(u'<?xml version="1.0" encoding="UTF-8" ?>\n\n'.encode("utf-8"))
    indent(textNode)
    ET.ElementTree(textNode).write(f, encoding="utf-8")

def writeXCESALIGN(cesAlignNode, fileName) :
  """ Write a "cesAlign" node (XCESALIGN format) into the file fileName"""
  with open(fileName, "w") as f :
    f.write(u'<?xml version="1.0" encoding="UTF-8" ?>\n'.encode("utf-8"))
    f.write(u'<!DOCTYPE cesAlign PUBLIC "-//CES//DTD XML cesAlign//EN" "">\n'.encode("utf-8"))
    indent(cesAlignNode)
    ET.ElementTree(cesAlignNode).write(f, encoding="utf-8")

def writeXCESALIGNFromListLink(listLink, fileName, type="sent", fromDoc=" ", toDoc=" ") :
  w = open(fileName, "w")
  cesAlignNode = ET.Element(u"cesAlign", attrib={u"fromDoc" : fromDoc, u"toDoc" : toDoc, u"type" : type, u"version" : u"1.13"})
  linkList = ET.SubElement(cesAlignNode, u"linkList")
  linkGrp = ET.SubElement(linkList, u"linkGrp", attrib={u"fromDoc" : fromDoc, u"toDoc" : toDoc})
  for l in listLink :
    IDsrc, IDtrg, cert = l
    link = ET.SubElement(linkGrp, u"link", attrib={u"certainty" : u"{0}".format(cert), u"xtargets" : u"{0};u{1}".format(IDsrc, IDtrg)})
  indent(cesAlignNode)
  w.write(u'<?xml version="1.0" encoding="UTF-8" ?>\n'.encode("utf-8"))
  w.write(u'<!DOCTYPE cesAlign PUBLIC "-//CES//DTD XML cesAlign//EN" "">\n'.encode("utf-8"))   
  ET.ElementTree(cesAlignNode).write(w, encoding="utf-8")
  
class xmlFile :

  """
  Basic xml processing : check whether the file exists; use basic parser
  to parse the document.
  """

  def __init__(self, fileName) :
    if not exists(fileName) or stat(fileName)[6] == 0:
      raise FileError(fileName)
    if not fileName.split('.')[-1] == "xml":
      raise FileTypeError(fileName, "Need a xml file.")
    self.fileName = fileName

  def parse(self) :
    try : tree = ET.parse(self.fileName);
    except ET.ParseError :
      print("%s ParseError : Continue with a less strict parser ? " % (self.fileName));
      res = raw_input("Y|N :  ");
      if not (res.startswith("Y") or res.startswith("y")) :
        raise FileTypeError(self.fileName, "This xml file cannot be parsed, please check its vadility.")
      parser = ET.xmlParser()
      parser.parser.UseForeignDTD(True)
      parser.entity = defaultdict(str)
      tree = ET.parse(self.fileName, parser=parser)

    return tree

class xcesTextFile(xmlFile) :

  """ 
  This class represents a text encoded in the XCES format. 
  It requires a xml document containing the tags "p", "s" and "w".
  XML entities are converted into unicode strings by the htmlUnescape method.
  Sentence index begins at 0.
  """

  def __init__(self, fileName) :
    xmlFile.__init__(self, fileName)
    self.dictSent = OrderedDict()
    self.numSent = 0
    self.wordCount = {}
    self.tree = self.parseFile()
    self.dictID2Ind = self.getSentIDIndex()
    self.dictInd2ID = self.getSentIndexID()
  
  def parseFile(self) :
    """
    Put paragraph, sentence and word information into the main dictionary
    """
    tree = xmlFile.parse(self)
    if tree.getiterator(u"p") == None :
      raise FileTypeError(self.fileName, \
        "This file does not contain any \"p\" element, perhaps it is not a xces file.")
    if tree.getiterator(u"s") == None :
      raise FileTypeError(self.fileName, \
        "This file does not contain any \"s\" element, perhaps it is not a xces file.")
    ind = 0
    for p in tree.getiterator(u"p") :
      paraID = p.get(u"id")
      if paraID == None : paraID = p.get(u"pid")
      self.dictSent[paraID] = OrderedDict()
      for s in p.getiterator(u"s") :
        sentID = s.get(u"id")
        if sentID == None : sentID = s.get("sid")
        sent = u" ".join([htmlUnescape(w.text).strip() for w in s.getiterator("w")])
        self.dictSent[paraID][sentID] = sent
        self.numSent += 1
        for word in sent.lower().split() :
          if word not in self.wordCount :
            self.wordCount[word] = 0.0
          self.wordCount[word] += 1
    return tree

  def getSentIDIndex(self) :
    """
    Return the index of each sentence in the document
    """
    dictSentIndex = OrderedDict()
    ind = 0
    for paraID in self.dictSent :
      for sentID in self.dictSent[paraID] :
        dictSentIndex[sentID] = ind
        ind += 1

    return dictSentIndex

  def getSentIndexID(self) :
    """
    Return the ID of each sentence in the document
    """
    dictSentID = {}
    ind = 0
    for paraID in self.dictSent :
      for sentID in self.dictSent[paraID] :
        dictSentID[ind] = sentID
        ind += 1

    return dictSentID

  def findSentenceNode(self, sentID) :
    """
    Find an s node by its ID 
    """
    for s in self.tree.findall(u"s"):
      sid = s.get(u"id")
      if sid == None : sid = s.get("sid")
      if sid == sentID : return s
    return None

  def writeLower(self, output) :
    w = open(output, "w")
    for pid in self.dictSent :
      for sid in self.dictSent[pid] :
        w.write(u"{0}\n".format(self.recoverSentence(sid)).lower().encode("utf-8"))
    w.close()

  def getSentenceDict(self) :
    """
    Create a dictionary, whose keys are sentence indexes,
    values are sentences.
    """
    dictSent = {}
    ind = 0
    for paraID in self.dictSent :
      for sentID in self.dictSent[paraID] :
        dictSent[ind] = self.dictSent[paraID][sentID].lower()
        ind += 1

    return dictSent

  def recoverSentence(self, sentID) :
    """
    Recover a sentence given its ID
    """
    paraID = sentID.split(".")[0]
    if paraID not in self.dictSent : return ""
    if sentID not in self.dictSent[paraID] : return ""
    return self.dictSent[paraID][sentID]

  def findSentenceByInd(self, ind) :
    """
    Recover a sentence given its index
    """
    try :
      ID = self.dictInd2ID[ind]
      return self.recoverSentence(ID)
    except : return ""

  def findParagraphNode(self, paraID) :
    """
    Find a p node by its ID
    """
    for p in self.tree.findall(u"p"):
      pid = p.get(u"id")
      if pid == None : pid = p.get("pid")
      if pid == paraID : return p
    return None

  def recoverParagraph(self, paraID) :
    """
    Recover a paragraph given its ID.
    Sentences are separated by a new line in the output string. 
    """
    if paraID not in self.dictSent : return None
    return u"\n".join([self.recoverSentence(sentID) for sentID in self.dictSent[paraID]])

class xcesAlignFile(xmlFile) :
  """
  This class represents a XCESALIGN format sentence alignment document.
  It requires a xml document containing the tag "link".

  A link is represented as a list of strings followed by a certainty score. The strings each contain 0, 1, or several
  sentence IDs, seperated by space. The first string contains IDs of the source side,
  and the second contains IDs of the target side. For example, a small alignment with 
  two links is described as
  [["1.1 1.2", "1.1", 0.03], ["1.3", "1.2 1.3", 0.05]]
  """

  def __init__(self, fileName) :
    xmlFile.__init__(self, fileName)
    self.tree = None
    self.level = None
    self.listLink = []
    self.sourceFile = u""
    self.targetFile = u""
    self.parseFile()

  def parseFile(self) :
    self.tree = xmlFile.parse(self)
    if self.tree.getiterator(u"cesAlign") == None :
      raise FileTypeError(self.fileName, \
        "This file does not contain any \"cesAlign\" element, perhaps it is not a XCESALIGN file.")    
    if self.tree.getiterator(u"link") == None :
      raise FileTypeError(self.fileName, \
        "This file does not contain any \"link\" element, perhaps it is not a XCESALIGN file.")  

    for cesAlign in self.tree.getiterator(u"cesAlign") :
      self.sourceFile = cesAlign.get(u"fromDoc")
      self.targetFile = cesAlign.get(u"toDoc")
      self.level = cesAlign.get(u"type")
      if self.level == None : self.level = u"sent"
    for link in self.tree.getiterator(u"link") :
      tmp = link.get(u"xtargets")
      IDsrc, IDtrg = tmp.split(u";")
      if link.get(u"certainty") != None :
        certainty = float(link.get(u"certainty"))
      elif link.get(u"prob") != None : 
        certainty = float(link.get(u"prob"))
      else : certainty = None
      self.listLink.append( [IDsrc, IDtrg, certainty] ) 

  def findIntersection(self, other) :
    inter = []
    for link in self.listLink :
      IDsrc, IDtrg, cert = link
      if IDsrc != "" : lastSrc = IDsrc.split()[-1]
      inOther = False
      for otherLink in other.listLink :
        otherIDsrc, otherIDtrg, otherCert = otherLink
        
        if IDsrc == otherIDsrc and IDtrg == otherIDtrg :
          inOther = True
      if inOther : 
        inter.append(link)
    return inter

  def writeReverse(self, reverseFileName) :
    tree = xmlFile.parse(self)

    for cesAlign in tree.getiterator("cesAlign") :
      cesAlign.set("fromDoc", self.targetFile)
      cesAlign.set("toDoc", self.sourceFile)
    for linkGrp in tree.getiterator("linkGrp") :
      linkGrp.set("fromDoc", self.targetFile)
      linkGrp.set("toDoc", self.sourceFile)
    for link in tree.getiterator("link") :
      tmp = link.get("xtargets")
      IDsrc, IDtrg = tmp.split(";")
      link.set("xtargets", u";".join([IDtrg, IDsrc]))

    with open(reverseFileName, 'w') as f:
      f.write('<?xml version="1.0" encoding="UTF-8" ?>\n'.encode("utf-8"))
      f.write('<!DOCTYPE cesAlign PUBLIC "-//CES//DTD XML cesAlign//EN" "">\n'.encode("utf-8"))
      tree.write(f, encoding="utf-8") 

if __name__ == "__main__" :
  pass
